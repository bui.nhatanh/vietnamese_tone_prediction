import re
import string

import unidecode
import numpy as np
from nltk import ngrams

MAXLEN = 30
NGRAM = 5
eng_alphabet = "abcdefghijklmnopqrstuvwxyz"
viet_alphabet = "áàảãạâấầẩẫậăắằẳẵặóòỏõọôốồổỗộơớờởỡợéèẻẽẹêếềểễệúùủũụưứừửữựíìỉĩịýỳỷỹỵđ"
digits = "0123456789"
punctuations = " _!\"\',\-\.:;?_\(\)\x00"

pattern = "^[" + "".join((eng_alphabet, viet_alphabet, digits, punctuations)) + "]+$"

accented_chars_vietnamese = [
    'á', 'à', 'ả', 'ã', 'ạ', 'â', 'ấ', 'ầ', 'ẩ', 'ẫ', 'ậ', 'ă', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ',
    'ó', 'ò', 'ỏ', 'õ', 'ọ', 'ô', 'ố', 'ồ', 'ổ', 'ỗ', 'ộ', 'ơ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ',
    'é', 'è', 'ẻ', 'ẽ', 'ẹ', 'ê', 'ế', 'ề', 'ể', 'ễ', 'ệ',
    'ú', 'ù', 'ủ', 'ũ', 'ụ', 'ư', 'ứ', 'ừ', 'ử', 'ữ', 'ự',
    'í', 'ì', 'ỉ', 'ĩ', 'ị',
    'ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ',
    'đ',
]
accented_chars_vietnamese.extend([c.upper() for c in accented_chars_vietnamese])
alphabet = list(('\x00 _' + string.ascii_letters + string.digits
                 + ''.join(accented_chars_vietnamese) + string.punctuation))


def remove_accent(text):
    return unidecode.unidecode(text)


def extract_phrases(text):
    return re.findall(r'\w[\w ]+', text)


def gen_ngrams(words, n=NGRAM):
    return ngrams(words.split(), n)


def encode(text, maxlen=MAXLEN):
    text = "\x00" + text
    x = np.zeros((maxlen, len(alphabet)))
    for i, c in enumerate(text[:maxlen]):
        x[i, alphabet.index(c)] = 1
    if i < maxlen - 1:
        for j in range(i + 1, maxlen):
            x[j, 0] = 1
    return x


def decode(x, calc_argmax=True):
    if calc_argmax:
        x = x.argmax(axis=-1)
    return ''.join(alphabet[i] for i in x)


def generate_data(data, batch_size=128):
    cur_index = 0
    while True:

        x, y = [], []
        for i in range(batch_size):
            y.append(encode(data[cur_index]))
            x.append(encode(unidecode.unidecode(data[cur_index])))
            cur_index += 1

            if cur_index > len(data) - 1:
                cur_index = 0

        yield np.array(x), np.array(y)
